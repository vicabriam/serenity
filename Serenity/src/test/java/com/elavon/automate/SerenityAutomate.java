package com.elavon.automate;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

import org.demoqa.steps.UserSteps;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class SerenityAutomate {
        @Managed
        public WebDriver webdriver;
        
        @Steps
        UserSteps user;
        
        @Ignore
        @Test
        public void TestDemo(){
            user.opens_home_page();
            user.clicks_registration();
            user.types_firstName();
            user.types_lastName();
            user.types_status();
            user.chooses_hobby2();
            user.chooses_hobby1();
            user.picks_country();
            user.picks_month();
            user.picks_day();
            user.picks_year();
            user.types_phoneNum();
            user.types_username();
            user.types_email();
            user.uploads_picture();
            user.types_about();
            user.types_password();
            user.confirms_password();
            user.clicks_submit();
        }
        
        @Test
        public void TestDemo2(){
            user.opens_home_page();
            user.clicks_registrationDelayed();
            user.gets_titleofpage();
            user.types_firstName();
            user.types_lastName();
            user.types_status();
            user.chooses_hobby2();
            user.chooses_hobby1();
            user.picks_country();
            user.picks_month();
            user.picks_day();
            user.picks_year();
            user.types_phoneNum();
            user.types_username();
            user.types_email();
            user.uploads_picture();
            user.types_about();
            user.types_password();
            user.confirms_password();
            user.clicks_submit();
        }
}
