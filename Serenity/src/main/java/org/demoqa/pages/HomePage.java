package org.demoqa.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demoqa.com")
public class HomePage extends PageObject {
    @FindBy(id="menu-item-374")
    private WebElementFacade regButton;
    
    public void clickRegistration(){
        regButton.waitUntilClickable().click();
    }
    
    @FindBy(id="name_3_firstname")
    private WebElementFacade firstName;
    
    public void typeFirstName(){
        firstName.waitUntilVisible().sendKeys("Hello");
    }
    
    @FindBy(id="name_3_lastname")
    private WebElementFacade lastName;
    
    public void typeLastName(){
        lastName.waitUntilVisible().sendKeys("World");
    }
    
    @FindBy(css="[name='radio_4[]'][value='divorced']")
    private WebElementFacade status;
    
    public void typeStatus(){
        status.waitUntilClickable().click();
    }
    
    @FindBy(css="[name='checkbox_5[]'][value='reading']")
    private WebElementFacade hobby1;
    
    public void chooseHobby1(){
        hobby1.waitUntilClickable().click();
    }
    
    @FindBy(css="[name='checkbox_5[]'][value='dance']")
    private WebElementFacade hobby2;
    
    public void chooseHobby2(){
        hobby2.waitUntilClickable().click();
    }
    
    @FindBy(id="dropdown_7")
    private WebElementFacade country;
    
    public void pickCountry(){
        country.waitUntilClickable().selectByVisibleText("Philippines");
    }
    
    @FindBy(id="mm_date_8")
    private WebElementFacade month;
    
    public void pickMonth(){
        month.waitUntilClickable().selectByVisibleText("4");
    }
    
    @FindBy(id="dd_date_8")
    private WebElementFacade day;
    
    public void pickDay(){
        day.waitUntilClickable().selectByVisibleText("14");
    }
    
    @FindBy(id="yy_date_8")
    private WebElementFacade year;
    
    public void pickYear(){
        year.waitUntilClickable().selectByVisibleText("2014");
    }
    
    @FindBy(id="phone_9")
    private WebElementFacade phone;
    
    public void typePhoneNumber(){
        phone.waitUntilVisible().sendKeys("09696969696");
    }
    
    @FindBy(id="username")
    private WebElementFacade username;
    
    public void typeUsername(){
        username.waitUntilVisible().sendKeys("helloworld69");
    }
    
    @FindBy(id="email_1")
    private WebElementFacade email;
    
    public void typeEmail(){
        email.waitUntilVisible().sendKeys("helloworld69@net.com");
    }
    
    @FindBy(id="profile_pic_10")
    private WebElementFacade picture;
    
    public void uploadPicture(){
        picture.waitUntilVisible().sendKeys("C:/Users/VCAbriam/Desktop/se_logo.jpg");
    }
    
    @FindBy(id="description")
    private WebElementFacade about;
    
    public void typeAbout(){
        about.waitUntilVisible().sendKeys("Hello Wold!");
    }
    
    @FindBy(id="password_2")
    private WebElementFacade password;
    
    public void typePassword(){
        password.waitUntilVisible().sendKeys("h3Ll0worLD69");
    }
    
    @FindBy(id="confirm_password_password_2")
    private WebElementFacade confirm;
    
    public void confirmPassword(){
        confirm.waitUntilVisible().sendKeys("h3Ll0worLD69");
    }
    
    @FindBy(name="pie_submit")
    private WebElementFacade submit;
    
    public void clickSubmit(){
        submit.waitUntilVisible().click();
    }
    
    @FindBy(id="menu-item-374")
    private WebElementFacade regButtonDelayed;
    
    public void clickRegistrationDelayed(){
        regButtonDelayed.waitUntilClickable().click();
    }
    
    @Override
    public String getTitle() {
        return getDriver().getTitle();
    }
    
    
}