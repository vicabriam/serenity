package org.demoqa.steps;

import net.thucydides.core.annotations.Step;

import org.demoqa.pages.HomePage;

public class UserSteps {
    HomePage home;
    
    @Step
    public void opens_home_page(){
        home.open();
    }
    
    @Step
    public void clicks_registration(){
        home.clickRegistration();
    }
    
    @Step
    public void types_firstName(){
        home.typeFirstName();
    }
    
    @Step
    public void types_lastName(){
        home.typeLastName();
    }
    
    @Step
    public void types_status(){
        home.typeStatus();
    }
    
    @Step
    public void chooses_hobby1(){
        home.chooseHobby1();
    }
    
    @Step
    public void chooses_hobby2(){
        home.chooseHobby2();
    }
    
    @Step
    public void picks_country(){
        home.pickCountry();
    }
    
    @Step
    public void picks_month(){
        home.pickMonth();
    }
    
    @Step
    public void picks_day(){
        home.pickDay();
    }
    
    @Step
    public void picks_year(){
        home.pickYear();
    }
    
    @Step
    public void types_phoneNum(){
        home.typePhoneNumber();
    }
    
    @Step
    public void types_username(){
        home.typeUsername();
    }
    
    @Step
    public void types_email(){
        home.typeEmail();
    }
    
    @Step
    public void uploads_picture(){
        home.uploadPicture();
    }
    
    @Step
    public void types_about(){
        home.typeAbout();
    }
    
    @Step
    public void types_password(){
        home.typePassword();
    }
    
    @Step
    public void confirms_password(){
        home.confirmPassword();
    }
    
    @Step
    public void clicks_submit(){
        home.clickSubmit();
    }
    
    @Step
    public void clicks_registrationDelayed(){
        home.waitFor(15).seconds().foo();
        home.clickRegistrationDelayed();
    }
    
    @Step
    public void gets_titleofpage(){
        System.out.println(home.getTitle());
    }
}
